package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private final EmployeeJPARepository employeeJPARepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository,EmployeeJPARepository employeeJPARepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<Employee> findAll() {
        return employeeJPARepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeJPARepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public void update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = findById(id);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        employeeJPARepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeJPARepository.findByGender(gender);
    }

    public Employee create(Employee employee) {
        return employeeJPARepository.save(employee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page-1, size);
        return employeeJPARepository.findAll(pageRequest).getContent();
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }

}
